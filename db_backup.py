from datetime import datetime
import subprocess
import boto3
import os
import asyncio
import time
import sys
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

db_connection_string=os.environ['DATABASE_URL']
region=''
access_key=os.environ['SPACES_KEY']
secret_access_key=os.environ['SPACES_SECRET']
bucket_url=os.environ['SPACES_URL']
backup_folder='dbbackups'
bucket_name='findmytea'

class GreenBack:
    last_backup_date = ""

    def callback(self, n, loop):
        today = datetime.now().strftime("%Y-%m-%d")
        if self.last_backup_date != today:
            logging.info('Backup started')
            destrination = self.backup()
            self.archive(destrination)
            
            self.last_backup_date = today
            logging.info('Backup finished')
            loop.call_at(loop.time() + n, self.callback, n, loop)

    def backup(self):    
        datestr = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
        backup_suffix = ".sql"
        backup_prefix = "backup_"

        destination = backup_prefix + datestr + backup_suffix
        backup_command = 'sh backup_command.sh ' + db_connection_string + ' ' + destination
        subprocess.check_output(backup_command.split(' '))
        return destination

    def archive(self, destination):
        session = boto3.session.Session()
        client = session.client('s3',
                                region_name=region,
                                endpoint_url=bucket_url,
                                aws_access_key_id=access_key,
                                aws_secret_access_key=secret_access_key)

        client.upload_file(destination, bucket_name, backup_folder + '/' + destination)
        os.remove(destination)    

event_loop = asyncio.get_event_loop()
try:
    bk = GreenBack()
    bk.callback(60, event_loop)
    event_loop.run_forever()
finally:
    logging.info('closing event loop')
    event_loop.close()